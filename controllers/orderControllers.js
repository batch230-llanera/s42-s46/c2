const User = require("../models/users.js");
const Product = require("../models/products.js");
const Order = require("../models/orders.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.addItemToCart = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to order.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let name = await Product.findById(request.body.productId).then(product => product.name);
		let price =  await Product.findById(request.body.productId).then(product => product.price)
		let stocks = await Product.findById(request.body.productId).then(product => product.stocks)

		let productId = request.body.productId
		let userId = userData.id
		let	email = userData.email
		let quantity = []

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
    		return response.status(400).send({ status: false, message: "Invalid user ID" })};

    	if (productId == null) {
    		return response.status(400).send({ status: false, message: "Invalid product" })};


    	let updateProductStocks = await Product.findByIdAndUpdate(productId).then(product => {
    		(product.stocks -= 1) -1
    		return product.save().then(result => {
    			console.log(result);
    			return true
    		})
    		.catch(error => {
    			console.log(error)
    			return false
    		})
    		console.log(updateProductStocks)
    		return response.send(updateProductStocks)
    	})


    	let cart = await Order.findOne({ userId: userId})

    	if (cart) {
    	    let itemIndex = cart.products.findIndex((p) => p.productId == productId);

    	    if (itemIndex > -1) {
    	      let productItem = cart.products[itemIndex];
    	      productItem.quantity += 1;
    	      productItem.stocks -= 1;
    	      productItem.subTotal = productItem.price * productItem.quantity;
    	      cart.products[itemIndex] = productItem;

    	      let totalAmount = cart.totalAmount + productItem.price
    	      cart.totalAmount = totalAmount
    	    } 
    	    else {
    	      let totalAmount = cart.totalAmount + price
    	      cart.totalAmount = totalAmount

    	      let subTotal = price
    	      stocks -= 1

    	      cart.products.push({ name: name, productId: productId, quantity: 1, stocks: stocks, price: price, subTotal : subTotal});
    	    }
    	    cart = await cart.save();
    	    return response.status(200).send({ status: true, updatedCart: cart });

    	    let updateProductStocks = await Product.findByIdAndUpdate(productId).then(product => {
    	    	product.stocks -=1
    	    	return product.save().then(result => {
    	    		console.log(result);
    	    		return true
    	    	})
    	    	.catch(error => {
    	    		console.log(error)
    	    		return false
    	    	})
    	    	console.log(updateProductStocks)
    	    	return response.send(updateProductStocks)
    	    })
    	  } 
    	  else {
    	  	let totalAmount = price
    	  	let subTotal = price

    	  	stocks -= 1

    	    const newCart = await Order.create({
    	      totalAmount,
    	      userId,
    	      products: [{ name: name, productId: productId, stocks: stocks, price: price, quantity: 1, subTotal: subTotal}],
    	    });

    	    return response.status(201).send({ status: true, newCart: newCart });

    
    	  }
    	
	}
}


module.exports.decreaseQuantity = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to edit order.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let productId = request.body.productId

		let userId = userData.id
		let	email = userData.email

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
    		return response.status(400).send({ status: false, message: "Please login to order." })};

    	let updateProductStocks = await Product.findByIdAndUpdate(productId).then(product => {
    		(product.stocks += 1) +1
    		return product.save().then(result => {
    			console.log(result);
    			return true
    		})
    		.catch(error => {
    			console.log(error)
    			return false
    		})
    		console.log(updateProductStocks)
    		return response.send(updateProductStocks)
    	})

    	let cart = await Order.findOne({ userId: userId})

    	if (cart == null){
    		return response.status(400).send({ status: false, message: "Cart is empty" })
    	}
    	else {
    		let itemIndex = cart.products.findIndex((p) => p.productId == productId);

    		if (itemIndex > -1) {
    		let productItem = cart.products[itemIndex];

    		productItem.quantity -= 1; 		
    		productItem.stocks += 1;
    	    productItem.subTotal = productItem.price * productItem.quantity;
    		cart.products[itemIndex] = productItem;
    		let totalAmount = cart.totalAmount - productItem.price
    	    cart.totalAmount = totalAmount

    	    if(productItem.quantity <= 0){cart.products.splice(itemIndex, 1);}
    		if(cart.totalAmount <= 0){(itemIndex, 1)}

    		cart = await cart.save();
    		return response.status(200).send({ status: true, updatedCart: cart });
  			}
  			response.status(400).send({ status: false, message: "Item does not exist in cart" });

    	}  	

	}
}

module.exports.viewCart = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to view cart.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let userId = userData.id
		let	email = userData.email

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
		    return response.status(400).send({ status: false, message: "Please login to view cart." })};

		let cart = await Order.findOne({ userId: userId})

		if (cart == null){
		    return response.status(400).send({ status: false, message: "Cart is empty" })
		}
		else{
			console.log(cart)
			response.send(cart)
		}

	}

}

module.exports.emptyCart = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to empty cart.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let userId = userData.id
		let	email = userData.email

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
		    return response.status(400).send({ status: false, message: "Please login to remove items in cart." })};

		let cart = await Order.findOne({ userId: userId})

		if (cart == null){
		    return response.status(400).send({ status: false, message: "Cart is empty" })
		}
		else{
			Order.findByIdAndRemove(request.params._id)
			.then(result => response.send(result))
			.catch(error => response.send(error));

		}

	}

}


module.exports.checkOut = async (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return response.status(402).send("Logged in as Admin. Please login as a User to complete the order.");
	}
	else{
		const userData = auth.decode(request.headers.authorization);

		let userId = userData.id
		let	email = userData.email

		let user = await User.exists({_id: userData.id});
		console.log(user)

		if (userId == null || user == null) {
		    return response.status(400).send({ status: false, message: "Please login to order." })};

		let cart = await Order.findOne({ userId: userId})

		if (cart == null){
		    return response.status(400).send({ status: false, message: "Cart is empty." })
		}
		else{
			Order.findByIdAndUpdate(request.params._id)

			cart.orderStatus = "order placed"	


			cart = await cart.save();
    		return response.status(200).send({ status: "completed", completedOrder: cart });
		}
		response.status(400).send({ status: false, message: "Order not placed" });

	    let updatedProduct = await Product.findByIdAndUpdate(products.productId).then(product => {
		products.stocks -= products.quantity;

		product.orders.push({
			orderIds: request.params._id
		})

		return product.save()
			.then(result => {
			console.log(result);
			return true
			})
			.catch(error => {
			console.log(error);
			return false
			})
			})

			console.log(updatedProduct)
			response.send(updatedProduct)
				
	}

}



module.exports.getAllOrders = (request, response) => {

	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return Order.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
	else{
		return response.status(402).send("Please login as a Admin to view orders.")
	}
}
