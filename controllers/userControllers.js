const User = require("../models/users.js");
const Product = require("../models/products.js");
const Order = require("../models/orders.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

module.exports.checkEmailAvailability = (request, response) => {
	return User.find({email: request.body.email}).then(result =>{
		console.log(result);
		if(result.length > 0){
			return response
			.send(true);
		}
		else{
			return response
			.send(false);
		}
	})
}

module.exports.userRegistration = (request, response) => {
	let newUser = new User({
		fullName: request.body.fullName,
		address: request.body.address,
		mobileNumber: request.body.mobileNumber,
		email: request.body.email,
		password: bcrypt.hashSync(request.body.password, 10)
	})

	console.log(newUser);

	return newUser.save()
	.then(user => {
		console.log(user);
		response.send(true);
	})
	.catch(error => {
		console.log(error);
		response.send(false);
	})

}

module.exports.userLogin = (request,response) => {
	return User.findOne({email: request.body.email})
	.then(result => {
		if(result == null){
			return response.send(false);
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			if(isPasswordCorrect){
				return response.send({accessToken: auth.createAccessToken(result)});
			}
			else{
				return response.send(false);
			}
		}
	})
}

	module.exports.getProfile = (req, res) => {
		
		const userData = auth.decode(req.headers.authorization);

		console.log(userData);

		return User.findById(userData.id).then(result =>{
			result.password = "***";
			res.send(result);
		})
	}


module.exports.getAllUsers = (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return User.find({})
		.then(result => response.send(result));
	}
	else{
		return response.status(402).send("Only admin can view all users.");
	}
}

module.exports.updateAccess = (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		let updateUserPriveleges = {
			isAdmin: request.body.isAdmin
			
		}

		return User.findByIdAndUpdate(request.params._id, updateUserPriveleges, {new:true})
		.then(result =>{
			console.log(result);
			response.send(result);
		})
		.catch(error =>{
			console.log(error);
			response.send(false);
		});
	}
	else{
		return response.status(402).send("Admin access required!");
	}
}

module.exports.deleteUser = (request, response) => {
	const checkAccess = auth.decode(request.headers.authorization).isAdmin;

	if(checkAccess == true){
		return User.findByIdAndRemove(request.params._id)
			.then(result => response.send(result))
			.catch(error => response.send(error));
	}
	else{
		return response.status(402).send("Admin access required!");
	}
}