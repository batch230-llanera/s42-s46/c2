const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	orderStatus: {
		type: String,
		default: "pending"
	},
	totalAmount: {
		type: Number,
		default: 0
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userInfo: [
		{
			name: {
			type: String,
			required: [true, "User ID is required"]
			},
			address: {
				type: String,
				required: [true, "Last name is required"]
			},
			mobileNumber: {
				type: String,
				required: [true, "Mobile number is required"]
			}
		}

	],
	products : [
		{
			name: {
				type: String,
				required: [true, "Product name is required"]
			},
			productId: {
				type: String,
				required: [true, "ProductId is required"]
			},
			price: {
				type: Number,
				required: [true, "Price is required"]
			},
			quantity: {
				type: Number,
				default: 0
			},
			subTotal: {
				type: Number,
			default: 0
			}

		}
	]
})

module.exports = mongoose.model("Order", orderSchema)

