const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	fullName: {
		type: String,
		required: [true, "First name is required"]
	},
	address: {
		type: String,
		required: [true, "Last name is required"]
	},
	mobileNumber: {
		type: String,
		required: [true, "Mobile number is required"]
	},
	email: {
		type: String,
		required: [true, "Email is required"]
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	createOn:{
		type: Date,
		default: new Date()
	}

})

module.exports = mongoose.model("User", userSchema)
