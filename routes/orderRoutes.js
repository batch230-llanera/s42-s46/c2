const express = require("express");
const router = express.Router();
const orderControllers = require("../controllers/orderControllers.js")
const auth = require("../auth.js");

//router.post("/checkout", auth.verify, orderControllers.order)

router.patch("/addToCart", auth.verify, orderControllers.addItemToCart)

router.patch("/decreaseQuantity", auth.verify, orderControllers.decreaseQuantity)

router.get("/viewCart", auth.verify, orderControllers.viewCart)

router.post("/checkOut/:_id", auth.verify, orderControllers.checkOut)

router.delete("/emptyCart/:_id", auth.verify, orderControllers.emptyCart)

router.get("/allOrders", auth.verify, orderControllers.getAllOrders);


module.exports = router;